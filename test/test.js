var assert = require('assert');

var game = {
    rolls: [],
    currentRoll: 0,

    score: function () {
        var totalScore = 0;
        var frameIndex = 0;
        console.log(this.rolls);
        for (var frame = 0; frame < 10; frame++) {
            if (this._isStrike(frameIndex)) {
                totalScore += 10 + this.rolls[frameIndex + 1] + this.rolls[frameIndex + 2];
                frameIndex++;
            } else if (this._isSpare(frameIndex)) {
                totalScore += 10 + this.rolls[frameIndex + 2];
                frameIndex += 2;
            } else {
                totalScore += this.rolls[frameIndex] + this.rolls[frameIndex + 1];
                frameIndex += 2;
            }
        }
        return totalScore;
    },

    roll: function (pins) {
        this.rolls[this.currentRoll++] = pins;
    },

    _isSpare: function (frameIndex) {
        return (this.rolls[frameIndex] + this.rolls[frameIndex + 1]) === 10;
    },

    _isStrike: function (frameIndex) {
        return this.rolls[frameIndex] === 10;
    }
};

function rollMany(numberOfRolls, pinsPerRoll) {
    for (var i = 0; i < numberOfRolls; i++) {
        game.roll(pinsPerRoll);
    }
}

function rollSpare(){
    game.roll(5);
    game.roll(5);
}

function rollStrike(){
    game.roll(10);
}

describe('Bowling Game Test', function () {

    beforeEach(function(){
        game.rolls = [];
        game.currentRoll = 0;
    });

    describe('An all gutters game', function () {
        beforeEach(function () {
            rollMany(20, 0);
        });
        it('should have a score of zero', function () {
            assert.equal(0, game.score());
        });
    });

    describe('An all ones game', function () {
        beforeEach(function () {
            rollMany(20, 1);
        });
        it('should have a score of twenty', function () {
            assert.equal(20, game.score());
        });
    });

    describe('One spare', function () {
        beforeEach(function () {
            rollSpare();
            game.roll(3);
            rollMany(17, 0);
        });
        it('should have a score of sixteen', function () {
            assert.equal(16, game.score());
        });
    });

    describe('One strike', function () {
        beforeEach(function () {
            rollStrike();
            game.roll(3);
            game.roll(4);
            rollMany(16, 0);
        });
        it('should have a score of twenty-four', function () {
            assert.equal(24, game.score());
        });
    });

    describe('Perfect game', function () {
        beforeEach(function () {
            rollMany(12, 10);
        });
        it('should have a score of three-hundred', function () {
            assert.equal(300, game.score());
        });
    });
});
